
function Get_Affiliate_Cookie( check_name ) {
    var a_all_cookies = document.cookie.split( ';' );
    var a_temp_cookie = '';
    var cookie_name = '';
    var cookie_value = '';
    var b_cookie_found = false;

    for ( var i = 0; i < a_all_cookies.length; i++ ) {
        a_temp_cookie = a_all_cookies[i].split( '=' );
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

        // if the extracted name matches passed check_name
        if ( cookie_name == check_name ){
            b_cookie_found = true;
            // we need to handle case where cookie has no value but exists (no = sign, that is):
            if ( a_temp_cookie.length > 1 )
            {
                cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
            }
            // note that in cases where cookie is initialized but no value, null is returned
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = '';
    }
    if ( !b_cookie_found ){
        return null;
    }
}

document.addEventListener("confirmationStepReservation", function(event){



    var affiliateCookie = Get_Affiliate_Cookie("caren-affiliate");
    var searchParams = new URLSearchParams(affiliateCookie);


    if (searchParams.has('tap_a') && searchParams.has('tap_s')) {

        const guid = event.detail.reservation.Guid;

        const classId = event.detail.reservation.Vehicle.ClassId;
        const name = event.detail.reservation.Vehicle.Name;
        const price = event.detail.reservation.Vehicle.Price;
    
    
        const totalPrice = event.detail.reservation.Vehicle.TotalPrice;
        const currency = event.detail.reservation.Currency;



        (function (t, a, p) {
            t.TapfiliateObject = a;
            t[a] = t[a] || function () {
                (t[a].q = t[a].q || []).push(arguments)
            }
        })(window, 'tap');

        tap('create', tms_success.tapfiliateId, { integration: "javascript" });

        var metaData = {
            classId: classId,
            car: name
        };

        tap('conversion', guid, totalPrice, {meta_data: metaData});

    }

});