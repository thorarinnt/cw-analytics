document.addEventListener("confirmationStepReservation", function(event){


   
    console.log('event.detail.reservation', event.detail.reservation)

    // Create a transaction
    ga("ecommerce:addTransaction", {
        id: event.detail.reservation.Guid,
        affiliation: event.detail.reservation.Rental.Name,
        revenue: event.detail.reservation.TotalPrice,
        shipping: "",
        tax: "",
        currency: event.detail.reservation.Currency,
    });

    if(event.detail.reservation.Vehicle.ClassId){
        // Add vehicle to transaction
        ga("ecommerce:addItem", {
            id: event.detail.reservation.Guid,
            name: event.detail.reservation.Vehicle.Name,
            sku: event.detail.reservation.Vehicle.ClassId,
            category: "Car",
            price: event.detail.reservation.Vehicle.Price, // or Total price
            quantity: "1",
            currency: event.detail.reservation.Currency,
        });
    }



    const insuranceValue = [];
    for (let i = 0; i < event.detail.reservation.Insurances.length; i++) {
      var days = (event.detail.reservation.Days > 0 ? event.detail.reservation.Days : 1);
      if (event.detail.reservation.Insurances[i].Price > 0) {
        var tempArr = {
          id: event.detail.reservation.Guid, // Transaction ID. Required.
          name: event.detail.reservation.Insurances[i].Name, // Product name. Required.
          sku: event.detail.reservation.Insurances[i].Id, // SKU/code.
          category: "Insurance", // Category or variation.
          price: event.detail.reservation.Insurances[i].TotalPrice, // (insuranceArray[i]['perday'] === false ? insuranceArray[i]['price'] : insuranceArray[i]['price'] *  days ).toString(), //'11.99',                 // Unit price.
          quantity: 1,
          currency: event.detail.reservation.Currency,
        };
        ga("ecommerce:addItem", tempArr);
      }
    }
  
  
    const extrasValue = [];
    for (let i = 0; i < event.detail.reservation.Extras.length; i++) {
      var days = (event.detail.reservation.Days > 0 ? event.detail.reservation.Days : 1);
      var tempObj = {
        id: event.detail.reservation.Guid, // Transaction ID. Required.
        name: event.detail.reservation.Extras[i].Name, // Product name. Required.
        sku: event.detail.reservation.Extras[i].Id, // SKU/code.
        category: "Extras", // Category or variation.
        price: event.detail.reservation.Extras[i].TotalPrice, // or .Price
        quantity: event.detail.reservation.Extras[i].Quantity, // Quantity.
        currency: event.detail.reservation.Currency,
      };
      ga("ecommerce:addItem", tempObj);
    }

    ga("ecommerce:send");

});