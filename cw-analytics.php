<?php
/*
 * Plugin Name: Caren Analytics
 * Version: 1.0.1
 * Requires PHP: 7.0
 */

// If this file is called directly, go away.
if ( ! defined( 'WPINC' ) ) {
	die;
}



add_action( 'wp_enqueue_scripts', 'my_analytics_assets' );
function my_analytics_assets() {
    wp_enqueue_script( 'cw-analytics-test' , plugins_url( '/js/confirmation.js' , __FILE__ ), array( ),
    '',
    false );
}




add_action( 'wp_enqueue_scripts', 'my_tapfiliate_assets' );
function my_tapfiliate_assets() {
    wp_register_script( 'caren-tapfiliate', 'https://tapfiliate.com/tapfiliate.js' );
    wp_enqueue_script('caren-tapfiliate');
    wp_enqueue_script( 'cw-tapfiliate-test', plugins_url( '/js/tapfiliate.js' , __FILE__ ), array( 'caren-tapfiliate' ),
    '',
    false );
}